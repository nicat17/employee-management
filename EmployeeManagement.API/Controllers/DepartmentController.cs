﻿using Microsoft.AspNetCore.Mvc;
using EmployeeManagement.API.Controllers.Base;
using EmployeeManagement.Core.Abstractions.Services;
using EmployeeManagement.Core.DTOs.Entities;
using EmployeeManagement.Core.Infrastructure.ResponseConfig.Base;
using EmployeeManagement.Core.Infrastructure.ResponseConfig;

namespace EmployeeManagement.API.Controllers
{
    public class DepartmentController : AncestorController
    {
        private readonly IDepartmentService _departmentService;

        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        /// <summary>
        /// Get all Departments in database
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>Department's list</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SuccessResponse<IEnumerable<DepartmentDto>>))]
        public async Task<IActionResult> All(CancellationToken cancellationToken)
        {
            return await ResultAsync(_departmentService.AllAsync(cancellationToken));
        }

        /// <summary>
        /// Create new Department
        /// </summary>
        /// <param name="department"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>Success status code</returns>
        /// <remarks>
        /// {
        ///     "name":"IT department"
        /// }
        /// </remarks>
        /// <response code="201">Returns the newly created Department</response>
        /// <response code="400">If the Department is null</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(SuccessResponse<DepartmentDto>))]
        public async Task<IActionResult> Create(DepartmentDto department, CancellationToken cancellationToken)
        {
            return await ResultAsync(_departmentService.CreateAsync(department, cancellationToken));
        }

        /// <summary>
        /// Delete Department By Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(Response))]
        public async Task<IActionResult> Delete(int? id, CancellationToken cancellationToken)
        {
            return await ResultAsync(_departmentService.DeleteAsync(id, cancellationToken));
        }

        /// <summary>
        /// Edit Department
        /// </summary>
        /// <param name="departmentDto"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>Success status code</returns>
        ///<remarks>
        ///{
        ///     "name":"IT department"
        /// }
        /// </remarks>
        /// <response code="202">Returns Edited Department</response>
        /// <response code="400">If the Department is null</response>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(SuccessResponse<DepartmentDto>))]
        public async Task<IActionResult> Edit(int? id, DepartmentDto departmentDto, CancellationToken cancellationToken)
        {
            return await ResultAsync(_departmentService.EditAsync(id, departmentDto, cancellationToken));
        }
    }
}
