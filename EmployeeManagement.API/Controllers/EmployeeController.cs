﻿using Microsoft.AspNetCore.Mvc;
using EmployeeManagement.API.Controllers.Base;
using EmployeeManagement.Core.Abstractions.Services;
using EmployeeManagement.Core.Infrastructure.ResponseConfig.Base;
using EmployeeManagement.Core.Infrastructure.ResponseConfig;
using EmployeeManagement.Core.DTOs.Entities;

namespace EmployeeManagement.API.Controllers
{
    public class EmployeeController : AncestorController
    {
        private readonly IEmployeeService _employeeService;
        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        /// <summary>
        /// Get all Employees
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>Employee's list</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SuccessResponse<IEnumerable<EmployeeDto>>))]
        public async Task<IActionResult> All(CancellationToken cancellationToken)
        {
            return await ResultAsync(_employeeService.AllAsync(cancellationToken));
        }


        /// <summary>
        /// Get all or any Employees with filter and pagination
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>Employee's list</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SuccessResponse<IEnumerable<EmployeeDto>>))]
        public async Task<IActionResult> Search([FromQuery] SearchEmployeeDto searchEmployeeDto, CancellationToken cancellationToken)
        {
            return await ResultAsync(_employeeService.SearchAsync(searchEmployeeDto, cancellationToken));
        }


        /// <summary>
        /// Create new Employee
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>Success status code</returns>
        ///<remarks>
        ///{
        ///     "name":"Nijat",
        ///     "surname":"Abdullaev",
        ///     "birthDate":"1999-08-17",
        ///     "departmentId" : 1
        /// }
        /// </remarks>
        /// <response code="201">Returns the newly created Employee</response>
        /// <response code="400">If the Employee is null</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(SuccessResponse<EmployeeDto>))]
        public async Task<IActionResult> Create(EmployeeDto employee, CancellationToken cancellationToken)
        {
            return await ResultAsync(_employeeService.CreateAsync(employee, cancellationToken));
        }


        /// <summary>
        /// Delete Employee By Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(Response))]
        public async Task<IActionResult> Delete(int? id, CancellationToken cancellationToken)
        {
            return await ResultAsync(_employeeService.DeleteAsync(id, cancellationToken));
        }


        /// <summary>
        /// Get Employee details by its Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Response))]
        public async Task<IActionResult> GetDetailsById(int? id, CancellationToken cancellationToken)
        {
            return await ResultAsync(_employeeService.GetEmployeeByIdAsync(id, cancellationToken));
        }

        /// <summary>
        /// Edit Employee
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>Success status code</returns>
        ///<remarks>
        ///{
        ///     "name":"Nijat",
        ///     "surname":"Abdullaev",
        ///     "birthDate":"1999-08-17",
        ///     "departmentId" : 1
        /// }
        /// </remarks>
        /// <response code="202">Returns Edited Employee</response>
        /// <response code="400">If the Employee is null</response>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(SuccessResponse<EmployeeDto>))]
        public async Task<IActionResult> Edit(int? id, EmployeeDto employee, CancellationToken cancellationToken)
        {
            return await ResultAsync(_employeeService.EditAsync(id, employee, cancellationToken));
        }
    }
}
