﻿using Microsoft.OpenApi.Models;
using EmployeeManagement.BL.AutoMappers;
using System.Reflection;

namespace EmployeeManagement.API.Configurations.Extentions
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddSwaggerExtension(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSwaggerGen(opt =>
            {
                opt.SwaggerDoc("v1", new OpenApiInfo()
                {
                    Version = "v1",
                    Description = "Developed by Nijat Abdullaev",
                    Title = "Employee Management"
                });

                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                opt.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
            });

            return serviceCollection;
        }

        public static IServiceCollection AddMappers(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddAutoMapper(opt => { 
             
                 opt.AddProfile<DtoMappings>();
            });

            return serviceCollection;
        }

        public static IServiceCollection AddCORSConfig(this IServiceCollection serviceCollection, string policyName)
        {
            serviceCollection.AddCors(opt =>
            {
                opt.AddPolicy(name: policyName, op =>
                {
                    op
                    .AllowAnyOrigin()
                        .AllowAnyHeader()
                            .AllowAnyMethod();
                });
            });

            return serviceCollection;
        }
    }
}
