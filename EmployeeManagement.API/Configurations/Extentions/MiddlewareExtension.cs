﻿using EmployeeManagement.API.Configurations.Middlewares;

namespace EmployeeManagement.API.Configurations.Extentions
{
    public static class MiddlewareExtension 
    {
        public static IApplicationBuilder UseAppExceptionHandler(this IApplicationBuilder applicationBuilder)
        {
            return applicationBuilder.UseMiddleware<ExceptionHandlerMiddleware>();
        }
    }
}
