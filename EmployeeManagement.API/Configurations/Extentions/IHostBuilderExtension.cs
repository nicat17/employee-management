﻿using Autofac.Extensions.DependencyInjection;
using Autofac;
using EmployeeManagement.BL.AutoFac;
using EmployeeManagement.DAL.AutoFac;

namespace EmployeeManagement.API.Configurations.Extentions
{
    public static class IHostBuilderExtension
    {
        public static IHostBuilder AddAutoFac(this IHostBuilder hostBuilder)
        {
            hostBuilder.UseServiceProviderFactory(new AutofacServiceProviderFactory())
            .ConfigureContainer<ContainerBuilder>(opt =>
            {
                opt.RegisterModule<RepoModule>();
                opt.RegisterModule<ServiceModule>();
            });

            return hostBuilder;
        }
    }
}
