using EmployeeManagement.API.Configurations.Extentions;
using EmployeeManagement.API.Configurations.Helpers;
using Microsoft.AspNetCore.Mvc.ApplicationModels;

const string ORIGIN_POLICY_NAME = "school_origin";

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers(options =>
{
    options.Conventions.Add(
        new RouteTokenTransformerConvention(
            new SlugifyParameterTransformer()));
});

// Add services to the container.
builder.Services.AddMappers();

builder.Host.AddAutoFac();

builder.Services.AddRouting(opt =>
{
    opt.LowercaseUrls = true;
    opt.LowercaseQueryStrings = true;
});

builder.Services.AddHttpContextAccessor();
builder.Services.AddSwaggerExtension();

builder.Services.AddCORSConfig(ORIGIN_POLICY_NAME);

WebApplication app = builder.Build();



// Configure the HTTP request pipeline.
app.UseAppExceptionHandler();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();

app.UseAuthorization();

app.UseCors(ORIGIN_POLICY_NAME);

app.MapControllers();

app.Run();