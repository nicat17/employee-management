﻿using EmployeeManagement.Core.DTOs.Entities;
using EmployeeManagement.Core.Entities;
using LinqKit;

namespace EmployeeManagement.DAL.Predicates
{
    public static class EmployeePredicate
    {
        public static ExpressionStarter<Employee> GetPredicateForFilteredEmployees(SearchEmployeeDto searchEmployeeDto)
        {
            var expressionStarter = PredicateBuilder.New<Employee>(true);

            if (string.IsNullOrEmpty(searchEmployeeDto.Name) is false)
                expressionStarter.And(x => x.Name.Contains(searchEmployeeDto.Name));

            if (string.IsNullOrEmpty(searchEmployeeDto.Surname) is false)
                expressionStarter.And(x => x.Surname.Contains(searchEmployeeDto.Surname));

            if (searchEmployeeDto.BirthDate.HasValue)
                expressionStarter.And(x => x.BirthDate.Date == searchEmployeeDto.BirthDate.Value.Date);

            if (string.IsNullOrEmpty(searchEmployeeDto.DepartmentName) is false)
                expressionStarter.And(x => x.Department.Name.Contains(searchEmployeeDto.DepartmentName));

            return expressionStarter;
        }
    }
}
