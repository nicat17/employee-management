﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using EmployeeManagement.Core.Abstractions;
using EmployeeManagement.Core.Abstractions.Repositories;
using EmployeeManagement.DAL.AppDbContext;

namespace EmployeeManagement.DAL.Repositories.Base
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SchoolDbContext _schoolDbContext;

        public UnitOfWork(SchoolDbContext schoolDbContext,
                          IEmployeeRepository employeeRepository,
                          IDepartmentRepository departmentRepository)
        {
            _schoolDbContext = schoolDbContext;
            EmployeeRepository= employeeRepository;
            DepartmentRepository= departmentRepository;
        }

        public IEmployeeRepository EmployeeRepository { get; private set; }

        public IDepartmentRepository DepartmentRepository { get; private set; }


        public async Task CommitAsync(CancellationToken cancellationToken = default)
        {
            ModifyContextChanges();
            await _schoolDbContext.SaveChangesAsync(cancellationToken);
        }

        /// <summary>
        /// If acceptAllChangesOnSuccess is true, then AcceptAllChanges() will be called and all changes will be accepted successfully.
        /// Otherwise, if it is false, then EF will be able to track same changes. 
        /// </summary>
        /// <param name="acceptAllChangesOnSuccess"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task CommitAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            ModifyContextChanges();
            await _schoolDbContext.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        public void Dispose()
        {
            _schoolDbContext.Dispose();
        }

        #region Private
        private void ModifyContextChanges()
        {
            _schoolDbContext.ChangeTracker
                .DetectChanges();

            foreach (EntityEntry entityEntry in _schoolDbContext.ChangeTracker.Entries())
            {
                if (entityEntry.Entity is IEntity entity)
                {
                    switch (entityEntry.State)
                    {
                        case EntityState.Deleted:
                            entity.IsDeleted = true;
                            entityEntry.State = EntityState.Modified;
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        #endregion
    }
}
