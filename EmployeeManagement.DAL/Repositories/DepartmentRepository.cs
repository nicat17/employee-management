﻿using EmployeeManagement.Core.Abstractions.Repositories;
using EmployeeManagement.Core.Entities;
using EmployeeManagement.DAL.AppDbContext;
using EmployeeManagement.DAL.Repositories.Base;

namespace EmployeeManagement.DAL.Repositories
{
    public class DepartmentRepository : Repository<Department>, IDepartmentRepository
    {
        public DepartmentRepository(SchoolDbContext schoolDbContext):base(schoolDbContext) { }
    }
}
