﻿using Microsoft.EntityFrameworkCore;
using EmployeeManagement.DAL.AppDbContext;
using EmployeeManagement.DAL.Repositories.Base;
using EmployeeManagement.Core.Entities;
using EmployeeManagement.Core.Abstractions.Repositories;
using EmployeeManagement.Core.DTOs.Entities;
using LinqKit;
using EmployeeManagement.DAL.Predicates;
using EmployeeManagement.Core.DTOs.Repo;

namespace EmployeeManagement.DAL.Repositories
{
    public class EmployeeRepository : Repository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(SchoolDbContext schoolDbContext) : base(schoolDbContext)
        {

        }

        public async Task<Employee> GetEmployeeWithDepartmentAsync(int id, CancellationToken cancellationToken = default)
        {
            return await _schoolDbContext.Set<Employee>()
                .Include(x => x.Department)
                        .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<EmployeeListRepoResultDto> GetFilteredEmployeesAsync(SearchEmployeeDto searchEmployeeDto,
                                                                           CancellationToken cancellationToken = default)
        {
            var expressionStarter = EmployeePredicate.GetPredicateForFilteredEmployees(searchEmployeeDto);

            var query = _schoolDbContext.Set<Employee>()
                .Include(x => x.Department)
                .AsExpandable()
                .Where(expressionStarter);

            int? page = searchEmployeeDto.Page;
            int? size = searchEmployeeDto.PageSize;

            int commonCount = await query.CountAsync(cancellationToken);

            if (page.HasValue && page.Value > 0 && size.HasValue && size.Value > 0)
                query = query.Skip((page.Value - 1) * size.Value).Take(size.Value);

            IEnumerable<Employee> employees = await query.ToListAsync(cancellationToken);

            return new EmployeeListRepoResultDto(employees, commonCount);
        }
    }
}
