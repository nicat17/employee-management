﻿using Autofac;
using EmployeeManagement.Core.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using EmployeeManagement.DAL.AppDbContext;
using EmployeeManagement.DAL.Repositories;
using EmployeeManagement.DAL.Repositories.Base;
using EmployeeManagement.Core.Abstractions.Repositories;

namespace EmployeeManagement.DAL.AutoFac
{
    public class RepoModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(n =>
            {
                IConfiguration configuration = n.Resolve<IConfiguration>();

                DbContextOptionsBuilder<SchoolDbContext> opt = new();

                opt.UseSqlServer(configuration.GetConnectionString("MSSQl_DB"));

                SchoolDbContext schoolDbContext = new (opt.Options);

                schoolDbContext.Database.Migrate();

                return schoolDbContext;

            }).InstancePerLifetimeScope();

            builder.RegisterType<EmployeeRepository>().As<IEmployeeRepository>().InstancePerLifetimeScope();
            builder.RegisterType<DepartmentRepository>().As<IDepartmentRepository>().InstancePerLifetimeScope();

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
        }
    }
}
