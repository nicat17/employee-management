﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using EmployeeManagement.Core.Abstractions;

namespace EmployeeManagement.DAL.Configurations.Base
{
    public abstract class EntityBaseConfig<TEntity> : IEntityConfig<TEntity>
        where TEntity : class, IEntity
    {
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("ID")
                .UseIdentityColumn(1, 1);

            builder.HasQueryFilter(x => x.IsDeleted == false);
        }
    }
}
