﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using EmployeeManagement.Core.Entities;
using EmployeeManagement.DAL.Configurations.Base;

namespace EmployeeManagement.DAL.Configurations
{
    public class DepartamentConfig : EntityBaseConfig<Department>
    {
        public override void Configure(EntityTypeBuilder<Department> builder)
        {
            builder.Property(x => x.Name)
                .HasColumnName("NAME")
                .HasMaxLength(100);

            builder.Property(x => x.CreateDate)
                .HasColumnName("CREATE_DATE");

            builder.HasMany(x => x.Employees)
                .WithOne(x => x.Department);

            builder.HasIndex(x => x.Name)
                .IsUnique().HasDatabaseName("UK_DEPARTMENT_NAME");

            builder.ToTable("EM_DEPARTMENTS");

            base.Configure(builder);
        }
    }
}
