﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using EmployeeManagement.Core.Entities;
using EmployeeManagement.DAL.Configurations.Base;

namespace EmployeeManagement.DAL.Configurations
{
    public class EmployeeConfig : EntityBaseConfig<Employee>
    {
        public override void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.Property(x=>x.Name).HasColumnName("NAME").HasMaxLength(50);
            builder.Property(x=>x.Surname).HasColumnName("SURNAME").HasMaxLength(50);
            builder.Property(x => x.CreateDate).HasColumnName("CREATE_DATE");
            builder.Property(x => x.BirthDate).HasColumnName("BIRTH_DATE").HasColumnType("date");

            builder.Property(x => x.DepartmentId).HasColumnName("DEPARTMENT_ID");

            builder.HasOne(x=>x.Department)
                .WithMany(x=>x.Employees)
                .HasForeignKey(x=>x.DepartmentId).OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_EMPLOYEES_DEPARTMENT_ID");

            builder.ToTable("EM_EMPLOYEES");

            base.Configure(builder);
        }
    }
}
