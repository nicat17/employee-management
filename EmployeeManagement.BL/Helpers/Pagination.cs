﻿using EmployeeManagement.Core.Abstractions;

namespace EmployeeManagement.BL.Helpers
{
    public class Pagination<T> : IPagination<T>
        where T : class
    {
        public IEnumerable<T> Values { get; init; }
        public int CurrentPage { get; private set; }
        public int PageSize { get; private set; }
        public int TotalPages { get; private set; }
        public int TotalItems { get; private set; }
        public bool HasPrevious => CurrentPage > 1;
        public bool HasNext => CurrentPage < TotalPages;

        public Pagination() { }
        

        public Pagination(IEnumerable<T> list, int currentPage, int pageSize, int count):this()
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalItems = count;
            TotalPages =  PageSize > 0 ? (int)Math.Ceiling(count / (double)PageSize) : 0;
            Values = list ?? new List<T>();
        }
    }
}
