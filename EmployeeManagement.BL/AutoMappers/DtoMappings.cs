﻿using AutoMapper;
using EmployeeManagement.Core.Entities;
using EmployeeManagement.Core.DTOs.Entities;

namespace EmployeeManagement.BL.AutoMappers
{
    public class DtoMappings : Profile
    {
        public DtoMappings()
        {
            CreateMap<Employee, EmployeeDto>()
                .ConstructUsing(x => new EmployeeDto(x.Name, x.Surname, x.BirthDate.ToShortDateString(),x.DepartmentId))
                .ForMember(x => x.BirthDate, opt => opt.MapFrom(x => x.BirthDate.ToShortDateString()))
                .ReverseMap();

            CreateMap<Employee, ReadEmployeeDto>()
                .ConstructUsing(x => new ReadEmployeeDto(x.Name, x.Surname, x.BirthDate.ToShortDateString(),
                        new DepartmentDto(x.Department != null ? x.Department.Name : string.Empty)))
                .ForMember(x => x.DepartmentDto, opt => opt.MapFrom(x => x.Department))
                .ForMember(x=>x.BirthDate, opt=> opt.MapFrom(x=> x.BirthDate.ToShortDateString()))
                .ReverseMap();

            CreateMap<Department, DepartmentDto>().ReverseMap();
        }
    }
}
