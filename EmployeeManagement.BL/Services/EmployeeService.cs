﻿using AutoMapper;
using EmployeeManagement.BL.Helpers;
using EmployeeManagement.BL.Validations;
using EmployeeManagement.Core.Abstractions;
using EmployeeManagement.Core.Abstractions.Services;
using EmployeeManagement.Core.Entities;
using EmployeeManagement.Core.Exceptions;
using EmployeeManagement.Core.Infrastructure.ResponseConfig;
using EmployeeManagement.Core.Infrastructure.ResponseConfig.Base;
using EmployeeManagement.Core.DTOs.Entities;
using System.Net;
using EmployeeManagement.SharedLibrary;
using EmployeeManagement.Core.DTOs.Repo;

namespace EmployeeManagement.BL.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public EmployeeService(IUnitOfWork unitOfWork,
                              IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> AllAsync(CancellationToken cancellationToken = default)
        {
            IEnumerable<Employee> employees = await _unitOfWork
                            .EmployeeRepository.GetAllAsync(cancellationToken);

            if (employees is null || employees.Any() is false)
                throw new DataNotFoundException();

            IEnumerable<EmployeeDto> employeeDtos = _mapper.Map<IEnumerable<EmployeeDto>>(employees);

            return new SuccessResponse<IEnumerable<EmployeeDto>>(employeeDtos);
        }

        public async Task<Response> SearchAsync(SearchEmployeeDto searchEmployeeDto, CancellationToken cancellationToken = default)
        {
            EmployeeListRepoResultDto repoResultDto = await _unitOfWork.EmployeeRepository
                .GetFilteredEmployeesAsync(searchEmployeeDto, cancellationToken);

            if (repoResultDto is null || repoResultDto.Employees is null || repoResultDto.Employees.Any() is false)
                throw new DataNotFoundException();

            IEnumerable<ReadEmployeeDto> employeeDtos = _mapper.Map<IEnumerable<ReadEmployeeDto>>(repoResultDto.Employees);

            Pagination<ReadEmployeeDto> pagination = new(employeeDtos,
                                                         searchEmployeeDto.Page.HasValue ? searchEmployeeDto.Page.Value : 0,
                                                         searchEmployeeDto.PageSize.HasValue ? searchEmployeeDto.PageSize.Value : 0,
                                                         repoResultDto.CommonCount);

            return new SuccessResponse<Pagination<ReadEmployeeDto>>(pagination);
        }

        public async Task<Response> CreateAsync(EmployeeDto employeeDto, CancellationToken cancellationToken = default)
        {
            if (employeeDto is null)
                throw new BadRequestException();

            Validator<EmployeeDtoValidator, EmployeeDto> validator = new(new EmployeeDtoValidator(), employeeDto);

            if (validator.IsValid is false)
                return new ErrorResponse(HttpStatusCode.BadRequest, validator.Errors);

            bool hasDepartment = await _unitOfWork.DepartmentRepository.AnyAsync(x => x.Id == employeeDto.DepartmentId);

            if (hasDepartment is false)
                return new ErrorResponse(ConstantLiterals.DepartmentNotFound);

            Employee employee = _mapper.Map<Employee>(employeeDto);

            await _unitOfWork.EmployeeRepository.AddAsync(employee, cancellationToken);

            await _unitOfWork.CommitAsync(cancellationToken);

            return new SuccessResponse<EmployeeDto>(employeeDto, HttpStatusCode.Created);
        }

        public async Task<Response> DeleteAsync(int? id, CancellationToken cancellationToken = default)
        {
            if (id is null || id < 1)
                throw new BadRequestException();

            Employee employee = await _unitOfWork
                .EmployeeRepository.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

            if (employee is null)
                throw new DataNotFoundException();

            _unitOfWork.EmployeeRepository.Delete(employee);

            await _unitOfWork.CommitAsync(cancellationToken);

            return new Response(HttpStatusCode.Accepted);
        }

        public async Task<Response> GetEmployeeByIdAsync(int? id, CancellationToken cancellationToken = default)
        {
            if (id is null || id < 1)
                throw new BadRequestException();

            Employee employee = await _unitOfWork
                .EmployeeRepository.GetEmployeeWithDepartmentAsync(id.Value, cancellationToken);

            if (employee is null)
                throw new DataNotFoundException();

            ReadEmployeeDto employeeRDto = _mapper.Map<ReadEmployeeDto>(employee);

            return new SuccessResponse<ReadEmployeeDto>(employeeRDto);
        }

        public async Task<Response> EditAsync(int? id, EmployeeDto employeeDto, CancellationToken cancellationToken = default)
        {
            if (id is null || id < 1)
                throw new BadRequestException();

            if (employeeDto is null)
                throw new BadRequestException();

            Validator<EmployeeDtoValidator, EmployeeDto> validator = new(new EmployeeDtoValidator(), employeeDto);

            if (validator.IsValid is false)
                return new ErrorResponse(HttpStatusCode.BadRequest, validator.Errors);

            bool hasDepartment = await _unitOfWork.DepartmentRepository.AnyAsync(x => x.Id == employeeDto.DepartmentId);

            if (hasDepartment is false)
                return new ErrorResponse(ConstantLiterals.DepartmentNotFound);

            Employee employee = await _unitOfWork.EmployeeRepository.FirstOrDefaultAsync(x => x.Id == id);

            if (employee is null)
                throw new DataNotFoundException();

            Employee newEmployee = _mapper.Map<Employee>(employeeDto);

            employee.Name = newEmployee.Name;
            employee.Surname = newEmployee.Surname;
            employee.BirthDate = newEmployee.BirthDate;
            employee.DepartmentId = newEmployee.DepartmentId;

            _unitOfWork.EmployeeRepository.Update(employee);

            await _unitOfWork.CommitAsync(cancellationToken);

            return new SuccessResponse<EmployeeDto>(employeeDto, HttpStatusCode.Accepted);
        }
    }
}
