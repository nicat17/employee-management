﻿using AutoMapper;
using EmployeeManagement.BL.Helpers;
using EmployeeManagement.BL.Validations;
using EmployeeManagement.Core.Abstractions;
using EmployeeManagement.Core.Abstractions.Services;
using EmployeeManagement.Core.DTOs.Entities;
using EmployeeManagement.Core.Entities;
using EmployeeManagement.Core.Exceptions;
using EmployeeManagement.Core.Infrastructure.ResponseConfig.Base;
using EmployeeManagement.Core.Infrastructure.ResponseConfig;
using System.Net;

namespace EmployeeManagement.BL.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DepartmentService(IUnitOfWork unitOfWork,
                              IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Response> AllAsync(CancellationToken cancellationToken = default)
        {
            IEnumerable<Department> departments = await _unitOfWork
                            .DepartmentRepository.GetAllAsync(cancellationToken);

            if (departments is null || departments.Any() is false)
                throw new DataNotFoundException();

            IEnumerable<DepartmentDto> departmentDtos = _mapper.Map<IEnumerable<DepartmentDto>>(departments);

            return new SuccessResponse<IEnumerable<DepartmentDto>>(departmentDtos);
        }

        public async Task<Response> CreateAsync(DepartmentDto departmentDto, CancellationToken cancellationToken = default)
        {
            if (departmentDto is null)
                throw new BadRequestException();

            Validator<DepartmentDtoValidator, DepartmentDto> validator = new(new DepartmentDtoValidator(), departmentDto);

            if (validator.IsValid is false)
                return new ErrorResponse(HttpStatusCode.BadRequest, validator.Errors);

            Department department = _mapper.Map<Department>(departmentDto);

            department.CreateDate = DateTime.Now;

            await _unitOfWork.DepartmentRepository.AddAsync(department, cancellationToken);

            await _unitOfWork.CommitAsync(cancellationToken);

            return new SuccessResponse<DepartmentDto>(departmentDto, HttpStatusCode.Created);
        }

        public async Task<Response> DeleteAsync(int? id, CancellationToken cancellationToken = default)
        {
            if (id is null || id < 1)
                throw new BadRequestException();

            Department department = await _unitOfWork
                .DepartmentRepository.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

            if (department is null)
                throw new DataNotFoundException();

            _unitOfWork.DepartmentRepository.Delete(department);

            await _unitOfWork.CommitAsync(cancellationToken);

            return new Response(HttpStatusCode.Accepted);
        }

        public async Task<Response> EditAsync(int? id, DepartmentDto departmentDto, CancellationToken cancellationToken = default)
        {
            if (id is null || id < 1)
                throw new BadRequestException();

            if (departmentDto is null)
                throw new BadRequestException();

            Validator<DepartmentDtoValidator, DepartmentDto> validator = new(new DepartmentDtoValidator(), departmentDto);

            if (validator.IsValid is false)
                return new ErrorResponse(HttpStatusCode.BadRequest, validator.Errors);

            Department department = await _unitOfWork.DepartmentRepository.FirstOrDefaultAsync(x => x.Id == id);

            if (department is null)
                throw new DataNotFoundException();

            Department newDepartment = _mapper.Map<Department>(departmentDto);

            department.Name = newDepartment.Name;

            _unitOfWork.DepartmentRepository.Update(department);

            await _unitOfWork.CommitAsync(cancellationToken);

            return new SuccessResponse<DepartmentDto>(departmentDto, HttpStatusCode.Accepted);
        }
    }
}
