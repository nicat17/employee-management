﻿using FluentValidation;
using EmployeeManagement.SharedLibrary;
using EmployeeManagement.Core.DTOs.Entities;

namespace EmployeeManagement.BL.Validations
{
    public class EmployeeDtoValidator : AbstractValidator<EmployeeDto>
    {
        public EmployeeDtoValidator()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().WithMessage(ConstantLiterals.NameCanNotBeEmpty);

            RuleFor(x => x.Surname).NotEmpty().NotNull().WithMessage(ConstantLiterals.SurnameCanNotBeEmpty);

            RuleFor(x => x.DepartmentId).GreaterThan(0).WithMessage(ConstantLiterals.DepartmentCanNotBeEmpty);
        }
    }
}
