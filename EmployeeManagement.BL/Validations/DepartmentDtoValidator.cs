﻿using EmployeeManagement.Core.DTOs.Entities;
using EmployeeManagement.SharedLibrary;
using FluentValidation;

namespace EmployeeManagement.BL.Validations
{
    public class DepartmentDtoValidator : AbstractValidator<DepartmentDto>
    {
        public DepartmentDtoValidator()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().WithMessage(ConstantLiterals.NameCanNotBeEmpty);
        }
    }
}
