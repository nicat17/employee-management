﻿using Autofac;
using EmployeeManagement.Core.Abstractions.Services;
using EmployeeManagement.BL.Services;

namespace EmployeeManagement.BL.AutoFac
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EmployeeService>().As<IEmployeeService>().InstancePerLifetimeScope();
            builder.RegisterType<DepartmentService>().As<IDepartmentService>().InstancePerLifetimeScope();
        }
    }
}
