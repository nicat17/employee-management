﻿using EmployeeManagement.Core.Abstractions;

namespace EmployeeManagement.Core.Entities
{
    public class Employee : IEntity
    {
        public int Id { get; init; }
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime CreateDate { get; set; }
        public Department Department { get; set; }
        public int DepartmentId { get; set; }
    }
}
