﻿using EmployeeManagement.Core.Abstractions;

namespace EmployeeManagement.Core.Entities.Base
{
    public abstract class EntityBase : IEntity
    {
        public int Id { get; init; }
        public bool IsDeleted { get; set; }
    }
}
