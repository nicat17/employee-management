﻿using EmployeeManagement.Core.Abstractions;

namespace EmployeeManagement.Core.Entities
{
    public class Department : IEntity
    {
        public int Id { get; init; }
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }

        public ICollection<Employee> Employees { get; set; }
    }
}
