﻿using EmployeeManagement.Core.Entities;

namespace EmployeeManagement.Core.Abstractions.Repositories
{
    public interface IDepartmentRepository : IRepository<Department>
    {

    }
}
