﻿using EmployeeManagement.Core.DTOs.Entities;
using EmployeeManagement.Core.DTOs.Repo;
using EmployeeManagement.Core.Entities;

namespace EmployeeManagement.Core.Abstractions.Repositories
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        Task<Employee> GetEmployeeWithDepartmentAsync(int id, CancellationToken cancellationToken = default);
        Task<EmployeeListRepoResultDto> GetFilteredEmployeesAsync(SearchEmployeeDto searchEmployeeDto, CancellationToken cancellationToken = default);
    }
}
