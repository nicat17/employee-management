﻿namespace EmployeeManagement.Core.Abstractions
{
    public interface IPagination<T>
        where T : class
    {
        IEnumerable<T> Values { get; init; }
        int CurrentPage { get; }
        int TotalPages { get; }
        int TotalItems { get; }
        bool HasPrevious { get; }
        bool HasNext { get; }
        int PageSize { get; }
    }
}
