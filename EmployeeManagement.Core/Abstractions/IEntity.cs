﻿namespace EmployeeManagement.Core.Abstractions
{
    public interface IEntity
    {
        public int Id { get; init; }
        public bool IsDeleted { get; set; }
    }
}
