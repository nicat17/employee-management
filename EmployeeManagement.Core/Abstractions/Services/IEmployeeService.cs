﻿using EmployeeManagement.Core.Infrastructure.ResponseConfig.Base;
using EmployeeManagement.Core.DTOs.Entities;

namespace EmployeeManagement.Core.Abstractions.Services
{
    public interface IEmployeeService
    {
        Task<Response> AllAsync(CancellationToken cancellationToken = default);
        Task<Response> SearchAsync(SearchEmployeeDto searchEmployeeDto, CancellationToken cancellationToken = default);
        Task<Response> CreateAsync(EmployeeDto employeeDto, CancellationToken cancellationToken = default);
        Task<Response> DeleteAsync(int? id, CancellationToken cancellationToken = default);
        Task<Response> GetEmployeeByIdAsync(int? id, CancellationToken cancellationToken = default);
        Task<Response> EditAsync(int? id, EmployeeDto employeeDto, CancellationToken cancellationToken = default);
    }
}
