﻿using EmployeeManagement.Core.DTOs.Entities;
using EmployeeManagement.Core.Infrastructure.ResponseConfig.Base;

namespace EmployeeManagement.Core.Abstractions.Services
{
    public interface IDepartmentService
    {
        Task<Response> AllAsync(CancellationToken cancellationToken = default);
        Task<Response> CreateAsync(DepartmentDto departmentDto, CancellationToken cancellationToken = default);
        Task<Response> DeleteAsync(int? id, CancellationToken cancellationToken = default);
        Task<Response> EditAsync(int? id, DepartmentDto departmentDto, CancellationToken cancellationToken = default);
    }
}
