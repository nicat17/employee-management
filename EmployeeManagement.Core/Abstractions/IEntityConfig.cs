﻿using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.Core.Abstractions
{
    public interface IEntityConfig<TEntity> : IEntityTypeConfiguration<TEntity> 
        where TEntity : class, IEntity
    {

    }
}
