﻿using EmployeeManagement.Core.Entities;

namespace EmployeeManagement.Core.DTOs.Repo
{
    public record EmployeeListRepoResultDto(IEnumerable<Employee> Employees, int CommonCount);
}
