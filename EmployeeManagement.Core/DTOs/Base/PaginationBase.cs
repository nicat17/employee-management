﻿namespace EmployeeManagement.Core.DTOs.Base
{
    public record PaginationBase
    {
        public int? Page { get; init; }
        public int? PageSize { get; init; }
    }
}
