﻿namespace EmployeeManagement.Core.DTOs.Entities
{
    public record DepartmentDto(string? Name);
}
