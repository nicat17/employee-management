﻿namespace EmployeeManagement.Core.DTOs.Entities
{
    public record ReadEmployeeDto(string Name, string Surname, string BirthDate, DepartmentDto DepartmentDto);
}
