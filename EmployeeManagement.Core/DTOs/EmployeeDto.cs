﻿namespace EmployeeManagement.Core.DTOs.Entities
{
    public record EmployeeDto(string Name, string Surname, string BirthDate, int DepartmentId);
}
