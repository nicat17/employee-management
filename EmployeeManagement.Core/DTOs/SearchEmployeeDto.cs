﻿using EmployeeManagement.Core.DTOs.Base;

namespace EmployeeManagement.Core.DTOs.Entities
{
    public record SearchEmployeeDto : PaginationBase
    {
        public string? Name { get; init; }
        public string? Surname { get; init; }
        public DateTime? BirthDate { get; init; }
        public string? DepartmentName { get; init; }
    }
}

